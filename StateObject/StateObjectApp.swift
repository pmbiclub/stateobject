//
//  StateObjectApp.swift
//  StateObject
//
//  Created by Artyom Romanchuk on 05.01.2021.
//

import SwiftUI

@main
struct StateObjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
