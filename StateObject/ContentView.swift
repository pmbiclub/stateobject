//
//  ContentView.swift
//  StateObject
//
//  Created by Artyom Romanchuk on 05.01.2021.
//

import SwiftUI

class Counter: ObservableObject {
    @Published var value: Int = 0
}

struct CounterView: View {
    @ObservedObject var counter1 = Counter() //так работать не будет если обновиться вью то во View CounterView тоже обновиться value потому что CounterView chieldconrol
    @StateObject var counter2 = Counter()
    
    var body: some View {
        
        VStack{
            VStack{
                Text("\(counter1.value)")
                Button("Counter View Increment"){
                    counter1.value += 1
                }
            }.padding(20)
            .background(Color.green)
            
            VStack{
                Text("\(counter2.value)")
                Button("Counter View StateObject"){
                    counter2.value += 1
                }
            }.padding(20)
            .background(Color.gray)
        }
        
    }
}


struct ContentView: View {
    @State private var count: Int = 0 //так работать не будет если обновиться вью то во View CounterView тоже обновиться value потому что CounterView chieldconrol
    
    var body: some View {
        VStack{
            Text("CONTENT VIEW")
            Text("\(count)")
            Button("Incremen ContentView Counter"){
                count += 1
            }
            
            
            CounterView()
        }.padding(20)
        .background(Color.yellow)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
